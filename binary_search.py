def binary_search(arr, target):
    low = 0
    high = len(arr) - 1

    while low <= high:
        mid = (low + high) // 2
        mid_val = arr[mid]

        if mid_val == target:
            return mid
        elif mid_val < target:
            low = mid + 1
        else:
            high = mid - 1

    return -1


# tartiblanmagan list
numbers = [1, 2, 14, 166, 4, 8, 12, ]
numbers.sort()  # binary search togri ishlashi uchun avval listni tartiblab olamiz
target_number = 12
result = binary_search(numbers, target_number)

if result != -1:
    print(f"{target_number} soni {result}-indexda")
else:
    print(f"{target_number} royxatdan topilmadi")

