class Voting:
    def __init__(self):
        self.voters_list = {'Jon': True, 'Sara': False}

    def vote(self, voter_name):
        voted = self.voters_list.get(voter_name)
        if voted is not None:
            if voted:
                print(f"{voter_name} siz bu saylovda ovoz bergansiz!")
            else:
                print(f"{voter_name} siz ovoz berishingiz mumkin.")
                self.voters_list[voter_name] = True
        else:
            self.voters_list[voter_name] = True
            print("Siz ro'yxatda yo'qsiz. Siz roʻyxatga qoʻshildingiz. Ovoz berishingiz mumkin.")


# Foydalanish
poll = Voting()
poll.vote('Jon')  # Output: Jon siz bu saylovda ovoz bergansiz!
poll.vote('Sara')  # Output: Sara siz ovoz berishingiz mumkin.
poll.vote('Mike')  # Output: Siz ro'yxatda yo'qsiz. Siz roʻyxatga qoʻshildingiz. Ovoz berishingiz mumkin.

