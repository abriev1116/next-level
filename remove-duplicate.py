def remove_duplicates(input_list):
    seen = set()
    result = []

    for d in input_list:
        # Har bir lug'atni xeshlash mumkin bo'lishi uchun frozensetga aylantiramiz
        # chunki lug'atlar mutable bolgani uchun hashlanmaydi
        frozenset_d = frozenset(d.items())

        if frozenset_d not in seen:
            seen.add(frozenset_d)
            result.append(d)

    return result


input_list = [
    {"key1": "value1"},
    {"k1": "v1", "k2": "v2", "k3": "v3"},
    {},
    {},
    {"key 1": " value 1"},
    {"key1": "value1"},
    {"key2": "value2"},
]

unique_list = remove_duplicates(input_list)
print(unique_list)
